//
//  ViewController.m
//  AutoLayout
//
//  Created by Ефанов Сергей on 10.08.14.
//  Copyright (c) 2014 Sergey Yefanov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
            
- (void)viewDidLoad
{
	
	[super viewDidLoad];

	UIView * view0 = [UIView new];
	view0.backgroundColor = [UIColor redColor];
	view0.translatesAutoresizingMaskIntoConstraints = NO;
	[self.view addSubview:view0];
	
	UIView * view1 = [UIView new];
	view1.backgroundColor = [UIColor blueColor];
	view1.translatesAutoresizingMaskIntoConstraints = NO;
	[self.view addSubview:view1];
	
	UIView * view2 = [UIView new];
	view2.backgroundColor = [UIColor greenColor];
	view2.translatesAutoresizingMaskIntoConstraints = NO;
	[self.view addSubview:view2];
	
	UIView * view3 = [UIView new];
	view3.backgroundColor = [UIColor brownColor];
	view3.translatesAutoresizingMaskIntoConstraints = NO;
	[self.view addSubview:view3];
	
	NSDictionary *views = NSDictionaryOfVariableBindings(view1, view2, view3, view0);
	
	NSString *str1 = @"|-10-[view0]-10-[view1(==view0)]-10-|";
	NSString *str2 = @"|-10-[view2]-10-[view3(==view2)]-10-|";
	NSString *str3 = @"V:|-10-[view0(==300)]-10-[view2]-10-|";
	NSString *str4 = @"V:|-10-[view1(==500)]-10-[view3]-10-|";
	
	NSArray * constrants1 = [NSLayoutConstraint constraintsWithVisualFormat:str1 options:NSLayoutFormatAlignAllBaseline metrics:nil views:views];
	
	NSArray * constrants2 = [NSLayoutConstraint constraintsWithVisualFormat:str2 options:NSLayoutFormatAlignAllBaseline metrics:nil views:views];
	
	NSArray * constrants3 = [NSLayoutConstraint constraintsWithVisualFormat:str3 options:0 metrics:nil views:views];
	
	NSArray * constrants4 = [NSLayoutConstraint constraintsWithVisualFormat:str4 options:0 metrics:nil views:views];
	
	NSMutableArray *mArr = [NSMutableArray new];
	
	[mArr addObjectsFromArray:constrants1];
	[mArr addObjectsFromArray:constrants2];
	[mArr addObjectsFromArray:constrants3];
	[mArr addObjectsFromArray:constrants4];
	
	
	[self.view addConstraints:mArr.copy];

}

@end
